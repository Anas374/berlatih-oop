<?php

require_once('animals.php');
require_once('frog.php');
require_once('ape.php');

$animals = new Animals("Shaun");
echo "Name : " . $animals->name . "<br>";
echo "Legs : " . $animals->legs . "<br>";
echo "Cold blooded : " . $animals->cold_blooded . "<br><br>";

$frog = new Frog("Buduk");
echo "Name : " . $frog->name . "<br>";
echo "Legs : " . $frog->legs . "<br>";
echo "Cold blooded : " . $animals->cold_blooded . "<br>";
echo $frog->Jump() . "<br><br>";

$ape = new Ape("Kera Sakti");
echo "Name : " . $ape->name . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "Cold blooded : " . $animals->cold_blooded . "<br>";
echo $ape->Yell();


?>